﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace FortySevenE
{
    [CustomEditor(typeof(FaceCapLiveModeReceiver))]
    public class FaceCapLiveModeReceiverEditor : Editor
    {
        private FaceCapLiveModeReceiver _script;

        private bool _showBlendshapeConfig = true;

        private readonly string[] faceCapBlendshapeNames = { "eyeBlinkRight", "eyeLookDownRight", "eyeLookInRight", "eyeLookOutRight", "eyeLookUpRight", "eyeSquintRight", "eyeWideRight", "eyeBlinkLeft", "eyeLookDownLeft", "eyeLookInLeft", "eyeLookOutLeft", "eyeLookUpLeft", "eyeSquintLeft", "eyeWideLeft", "jawForward", "jawRight", "jawLeft", "jawOpen", "mouthClose", "mouthFunnel", "mouthPucker", "mouthRight", "mouthLeft", "mouthSmileRight", "mouthSmileLeft", "mouthFrownRight", "mouthFrownLeft", "mouthDimpleRight", "mouthDimpleLeft", "mouthStretchRight", "mouthStretchLeft", "mouthRollLower", "mouthRollUpper", "mouthShrugLower", "mouthShrugUpper", "mouthPressRight", "mouthPressLeft", "mouthLowerDownRight", "mouthLowerDownLeft", "mouthUpperUpRight", "mouthUpperUpLeft", "browDownRight", "browDownLeft", "browInnerUp", "browOuterUpRight", "browOuterUpLeft", "cheekPuff", "cheekSquintRight", "cheekSquintLeft", "noseSneerRight", "noseSneerLeft", "tongueOut", "none" };
        //string[] polywinkBlendshapeNames = { "browInnerUp", "browDownLeft", "browDownRight", "browOuterUpLeft", "browOuterUpRight", "eyeLookUpLeft", "eyeLookUpRight", "eyeLookDownLeft", "eyeLookDownRight", "eyeLookInLeft", "eyeLookInRight", "eyeLookOutLeft", "eyeLookOutRight", "eyeBlinkLeft", "eyeBlinkRight", "eyeSquintLeft", "eyeSquintRight", "eyeWideLeft", "eyeWideRight", "cheekPuff", "cheekSquintLeft", "cheekSquintRight", "noseSneerLeft", "noseSneerRight", "jawOpen", "jawForward", "jawLeft", "jawRight", "mouthFunnel", "mouthPucker", "mouthLeft", "mouthRight", "mouthRollUpper", "mouthRollLower", "mouthShrugUpper", "mouthShrugLower", "mouthClose", "mouthSmileLeft", "mouthSmileRight", "mouthFrownLeft", "mouthFrownRight", "mouthDimpleLeft", "mouthDimpleRight", "mouthUpperUpLeft", "mouthUpperUpRight", "mouthLowerDownLeft", "mouthLowerDownRight", "mouthPressLeft", "mouthPressRight", "mouthStretchLeft", "mouthStretchRight", "tongueOut", "none" };

        void OnEnable()
        {
            _script = (FaceCapLiveModeReceiver)target;
        }

        public override void OnInspectorGUI()
        {

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            GameObject blendshapeMesh = _script.blendshapeMesh;
            UnrealLiveLinkReceiver liveLinkReceiver = _script.liveLinkReceiver;

            liveLinkReceiver = EditorGUILayout.ObjectField("Live Link Receiver", liveLinkReceiver, typeof(UnrealLiveLinkReceiver), true) as UnrealLiveLinkReceiver;
            blendshapeMesh = EditorGUILayout.ObjectField("Blendshape Mesh", blendshapeMesh, typeof(GameObject), true) as GameObject;

            if (liveLinkReceiver != _script.liveLinkReceiver)
            {
                _script.liveLinkReceiver = liveLinkReceiver;
                EditorUtility.SetDirty(_script);
            }

            if (blendshapeMesh != _script.blendshapeMesh)
            {
                _script.blendshapeMesh = blendshapeMesh;
                EditorUtility.SetDirty(_script);
            }

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            bool usePositionData = _script.usePositionData;
            bool useRotationData = _script.useRotationData;

            usePositionData = EditorGUILayout.Toggle("Use Position Data", usePositionData);
            useRotationData = EditorGUILayout.Toggle("Use Rotation Data", useRotationData);

            if (usePositionData != _script.usePositionData || useRotationData != _script.useRotationData)
            {
                _script.usePositionData = usePositionData;
                _script.useRotationData = useRotationData;
                EditorUtility.SetDirty(_script);
            }

            if (usePositionData || useRotationData)
            {
                EditorGUILayout.Space();

                Transform headTransform = _script.headTransform;

                headTransform = EditorGUILayout.ObjectField("Head Transform", headTransform, typeof(Transform), true) as Transform;
                if (headTransform != _script.headTransform)
                {
                    _script.headTransform = headTransform;
                    EditorUtility.SetDirty(_script);
                }

                bool neckTransformEnabled = _script.neckTransformEnabled;
                neckTransformEnabled = EditorGUILayout.Toggle("Neck", neckTransformEnabled);
                if (neckTransformEnabled != _script.neckTransformEnabled)
                {
                    _script.neckTransformEnabled = neckTransformEnabled;
                    EditorUtility.SetDirty(_script);
                }

                if (neckTransformEnabled)
                {
                    Transform neckTransform = _script.neckTransform;
                    float neckTransformBlendFactor = _script.neckTransformBlendFactor;

                    neckTransform = EditorGUILayout.ObjectField("Neck Transform", neckTransform, typeof(Transform), true) as Transform;
                    neckTransformBlendFactor = EditorGUILayout.FloatField("Neck Blend", neckTransformBlendFactor);

                    if (neckTransform != _script.neckTransform || neckTransformBlendFactor != _script.neckTransformBlendFactor)
                    {
                        _script.neckTransform = neckTransform;
                        _script.neckTransformBlendFactor = neckTransformBlendFactor;
                        EditorUtility.SetDirty(_script);
                    }
                }

                bool spineTransformEnabled = _script.spineTransformEnabled;
                spineTransformEnabled = EditorGUILayout.Toggle("Spine", spineTransformEnabled);
                if (spineTransformEnabled != _script.spineTransformEnabled)
                {
                    _script.spineTransformEnabled = spineTransformEnabled;
                    EditorUtility.SetDirty(_script);
                }

                if (spineTransformEnabled)
                {
                    Transform spineTransform = _script.spineTransform;
                    float spineTransformBlendFactor = _script.spineTransformBlendFactor;

                    spineTransform = EditorGUILayout.ObjectField("Spine Transform", spineTransform, typeof(Transform), true) as Transform;
                    spineTransformBlendFactor = EditorGUILayout.FloatField("Spine Blend", spineTransformBlendFactor);

                    if (spineTransform != _script.spineTransform || spineTransformBlendFactor != _script.spineTransformBlendFactor)
                    {
                        _script.spineTransform = spineTransform;
                        _script.spineTransformBlendFactor = spineTransformBlendFactor;
                        EditorUtility.SetDirty(_script);
                    }
                }
            }

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            bool useEyeDirectionData = _script.useEyeDirectionData;
            useEyeDirectionData = EditorGUILayout.Toggle("Use Eye Direction Data", useEyeDirectionData);
            if (useEyeDirectionData != _script.useEyeDirectionData)
            {
                _script.useEyeDirectionData = useEyeDirectionData;
                EditorUtility.SetDirty(_script);
            }

            if (useEyeDirectionData)
            {
                EditorGUILayout.Space();

                Transform leftEyeTransform = _script.leftEyeTransform;
                Transform rightEyeTransform = _script.rightEyeTransform;

                leftEyeTransform = EditorGUILayout.ObjectField("Left Eye Transform", leftEyeTransform, typeof(Transform), true) as Transform;
                rightEyeTransform = EditorGUILayout.ObjectField("Right Eye Transform", rightEyeTransform, typeof(Transform), true) as Transform;

                if (leftEyeTransform != _script.leftEyeTransform || rightEyeTransform != _script.rightEyeTransform)
                {
                    _script.leftEyeTransform = leftEyeTransform;
                    _script.rightEyeTransform = rightEyeTransform;
                    EditorUtility.SetDirty(_script);
                }
            }

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            #region Blendshape Config
            _showBlendshapeConfig = EditorGUILayout.Foldout(_showBlendshapeConfig, "Blendshape configuration:");

            if (blendshapeMesh != null)
            {
                if (_showBlendshapeConfig)
                {
                    SkinnedMeshRenderer smr = blendshapeMesh.GetComponent<SkinnedMeshRenderer>();
                    if (!smr)
                    {
                        GUILayout.BeginHorizontal(EditorStyles.helpBox);
                        EditorGUILayout.LabelField("Warning: Assigned blendshape mesh has no skinned mesh renderer.");
                        GUILayout.EndHorizontal();
                        return;
                    }

                    if (smr.sharedMesh.blendShapeCount == 0)
                    {
                        GUILayout.BeginHorizontal(EditorStyles.helpBox);
                        EditorGUILayout.LabelField("Warning: Assigned blendshape mesh has no blendshapes.");
                        GUILayout.EndHorizontal();
                        return;
                    }

                    int[] blendShapeIndexes = _script.blendShapeIndexes;

                    if (blendShapeIndexes == null)
                    {
                        blendShapeIndexes = InitializeBlendshapes(smr);
                        blendShapeIndexes = AutoConfigureBlendshapes(smr);

                        if (blendShapeIndexes != _script.blendShapeIndexes)
                        {
                            _script.blendShapeIndexes = blendShapeIndexes;
                            EditorUtility.SetDirty(_script);
                        }
                    }

                    for (int i = 0; i < blendShapeIndexes.Length; i++)
                    {
                        GUILayout.BeginHorizontal(EditorStyles.label);
                        string blendShapeName = smr.sharedMesh.GetBlendShapeName(i);
                        blendShapeIndexes[i] = EditorGUILayout.Popup(blendShapeName, blendShapeIndexes[i], faceCapBlendshapeNames);

                        if (blendShapeIndexes[i] != _script.blendShapeIndexes[i])
                        {
                            _script.blendShapeIndexes[i] = blendShapeIndexes[i];
                            EditorUtility.SetDirty(_script);
                        }

                        GUILayout.EndHorizontal();
                    }

                    GUILayout.BeginHorizontal(EditorStyles.helpBox);
                    if (GUILayout.Button("Initialize"))
                    {
                        blendShapeIndexes = InitializeBlendshapes(smr);
                        if (blendShapeIndexes != _script.blendShapeIndexes)
                        {
                            _script.blendShapeIndexes = blendShapeIndexes;
                            EditorUtility.SetDirty(_script);
                        }
                    }
                    if (GUILayout.Button("Automatic"))
                    {
                        blendShapeIndexes = AutoConfigureBlendshapes(smr);
                        if (blendShapeIndexes != _script.blendShapeIndexes)
                        {
                            _script.blendShapeIndexes = blendShapeIndexes;
                            EditorUtility.SetDirty(_script);
                        }
                    }
                    GUILayout.EndHorizontal();
                }
            }
            else
            {
                GUILayout.BeginHorizontal(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Warning: Blendshape mesh not assigned.");
                GUILayout.EndHorizontal();
                return;
            }
            #endregion

            EditorGUILayout.Space();
        }

        int[] InitializeBlendshapes(SkinnedMeshRenderer smr)
        {
            int[] blendShapeIndexes = new int[smr.sharedMesh.blendShapeCount];

            for (int i = 0; i < blendShapeIndexes.Length; i++)
            {
                blendShapeIndexes[i] = faceCapBlendshapeNames.Length - 1;
            }

            return blendShapeIndexes;
        }

        int[] AutoConfigureBlendshapes(SkinnedMeshRenderer smr)
        {
            int[] blendShapeIndexes = new int[smr.sharedMesh.blendShapeCount];

            for (int i = 0; i < blendShapeIndexes.Length; i++)
            {
                string name = smr.sharedMesh.GetBlendShapeName(i);

                for (int j = 0; j < faceCapBlendshapeNames.Length; j++)
                {
                    float nameDistance = ComputeLevenshteinDistance(faceCapBlendshapeNames[j].ToLower(), name) / (float)Mathf.Max(name.Length, faceCapBlendshapeNames[j].Length);
                    Debug.Log($"{name} and {faceCapBlendshapeNames[j]} distance: {nameDistance}");
                    if (nameDistance < 0.5)
                    {
                        blendShapeIndexes[i] = j;
                    }
                }
            }
            return blendShapeIndexes;
        }

        // https://stackoverflow.com/questions/6944056/c-sharp-compare-string-similarity
        public static int ComputeLevenshteinDistance(string s, string t)
        {
            if (string.IsNullOrEmpty(s))
            {
                if (string.IsNullOrEmpty(t))
                    return 0;
                return t.Length;
            }

            if (string.IsNullOrEmpty(t))
            {
                return s.Length;
            }

            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // initialize the top and right of the table to 0, 1, 2, ...
            for (int i = 0; i <= n; d[i, 0] = i++) ;
            for (int j = 1; j <= m; d[0, j] = j++) ;

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                    int min1 = d[i - 1, j] + 1;
                    int min2 = d[i, j - 1] + 1;
                    int min3 = d[i - 1, j - 1] + cost;
                    d[i, j] = Math.Min(Math.Min(min1, min2), min3);
                }
            }
            return d[n, m];
        }

    }
}